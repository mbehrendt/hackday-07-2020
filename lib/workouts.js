const generateSetsBasedOnMax = (max) => {
  const pyramid = [
    { reps: 5, porm: 25 },
    { reps: 3, porm: 50 },
    { reps: 2, porm: 75 },
    { reps: 1, porm: 85 },
    { reps: 1, porm: 101 },
  ]

  return pyramid.map(p => (
    `${p.reps} x ${nearestFive(max/100*p.porm)}kg`
  ))
}

const nearestFive = (value) => (
  Math.ceil((value) / 5) * 5
)

const generateSetsFor5By5 = (max) => {
  let sets = []

  for(let i = 1; i < 5; i++) {
    sets.push(`5 x ${nearestFive(max)}kg`)
  }

  return sets
}

export const generateBackWorkout = (maxes) => {
  return [
    {
      lift: 'Deadlift',
      sets: generateSetsBasedOnMax(maxes.deadlift)
    },
    {
      lift: 'Bent over row',
      sets: generateSetsFor5By5(maxes.deadlift/100*60)
    },
    {
      lift: 'T-bar row',
      sets: generateSetsFor5By5(maxes.deadlift/100*40)
    }
  ]
}

export const generateLegWorkout = (maxes) => {
  return [
    {
      lift: 'Squat',
      sets: generateSetsBasedOnMax(maxes.squat)
    },
    {
      lift: 'Leg press',
      sets: generateSetsFor5By5(maxes.squat*2)
    },
    {
      lift: 'Front Squat',
      sets: generateSetsFor5By5(maxes.deadlift/100*60)
    }
  ]
}

export const generateChestWorkout = (maxes) => {
  return [
    {
      lift: 'Bench press',
      sets: generateSetsBasedOnMax(maxes.bench)
    },
    {
      lift: 'Overhead press',
      sets: generateSetsBasedOnMax(maxes.ohp)
    },
    {
      lift: 'Dumbbell press',
      sets: generateSetsFor5By5(maxes.bench/3)
    },
  ]
}
