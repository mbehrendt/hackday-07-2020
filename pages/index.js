import Head from 'next/head'
import Link from 'next/link'

export default function Home({ employees, technologies }) {
  return (
    <>
      <Head>
        <title>Hackday</title>
      </Head>

      <div className="sm:w-2/5 flex flex-col items-center sm:items-start text-center sm:text-left">
        <h1 className="uppercase text-6xl text-blue-900 font-bold leading-none tracking-wide mb-2">SNAP</h1>
        <h2 className="uppercase text-4xl text-orange-500 text-secondary tracking-widest mb-6">Your personalised HIT workout plan</h2>
        <p className="text-gray-600 leading-relaxed mb-12">
          Our clever algorithm will create a personal workout plan for you, based on the HIT principals.
        </p>
        <Link href="/wizard">
          <a className="bg-purple-300 hover:bg-purple-400 py-3 px-6 uppercase text-lg font-bold text-white rounded-full">Get started</a>
        </Link>
      </div>

      <div className="mb-16 sm:mb-0 mt-8 sm:mt-0 sm:w-3/5 sm:pl-12">
        <img src="https://www.studentcentral.london/pageassets/energybase/mpbanner_01.png" />
      </div>
    </>
  )
}
