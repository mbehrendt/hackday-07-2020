import withAuth from 'components/with-auth'

export function Account() {
  return (
    <h1>Your account</h1>
  )   
}

export default withAuth(Account)