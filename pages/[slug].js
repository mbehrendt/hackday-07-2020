import Head from 'next/head'
import { request } from '../lib/cms'

export default function Home({ employee }) {
  return (
    <div>
      <Head>
        <title>{employee.name}</title>
      </Head>

      <h2>{employee.name}</h2>
    </div>
  )
}

const EMPLOYEE_QUERY = `query Employee($slug: String) {
  employee(filter: { slug: { eq: $slug }}) {
    name
  }
}`

const LIST_QUERY = `query List {
  allEmployees {
    slug
  }
}`

export async function getStaticPaths() {
  const data = await request({
    query: LIST_QUERY,
  })

  return {
    paths: data.allEmployees.map(e => (
      { params: { slug: e.slug }}
    )),
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const { employee } = await request({
    query: EMPLOYEE_QUERY,
    variables: { slug: params.slug }
  })

  return {
    props: { employee }
  }
}
