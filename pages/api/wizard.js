import auth0 from 'lib/auth0'
import { query as q } from 'faunadb'
import { client } from 'lib/db'
import { generateBackWorkout, generateLegWorkout, generateChestWorkout } from 'lib/workouts'

const schedule = {
  1: generateBackWorkout,
  2: generateLegWorkout,
  3: generateChestWorkout
}

const generatePlan = ({ days, frequency, maxes }) => {
  let plan = []
  let current = 1

  Object.keys(days).forEach((day) => {
    if (days[day]) {
      plan.push({
        day: day,
        lifts: schedule[current](maxes)
      })

      current += 1
    }
  })

  return plan
}

export default async (req, res) => {
  const data = JSON.parse(req.body)
  const session = await auth0.getSession(req)

  if (!session || !session.user) {
    return res.status(401).end('Not authenticated')
  }

  data.user = session.user.sub

  const plan = generatePlan(data)

  await client.query(
    q.Create(q.Collection('plans'), { data })
  )

  res.json({ plan })
}
