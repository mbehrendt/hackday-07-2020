import Head from 'next/head'
import { request } from 'lib/cms'

export default function Home({ employees, technologies }) {
  return (
    <>
      <Head>
        <title>Technologies</title>
      </Head>

      <div className="container">
        <h1 className="font-mono text-lg text-center mt-10 mb-10">Technologies used</h1>

        <div className="flex flex-wrap">
          {technologies.map(tech => (
            <div key={tech.id} className="w-1/3 mb-12 text-center">
              <img className="rounded-full mx-auto" width={100} src={tech.logo.url} />
              <h4 className="font-mono text-gray-600 mt-3">{tech.name}</h4>
            </div>
          ))}  
        </div>
      </div>
    </>
  )
}

const TECH_QUERY = `query {
  allTechnologies {
    id
    name
    logo {
      url
    }
  }
}`

export async function getStaticProps() {
  const { allTechnologies } = await request({
    query: TECH_QUERY
  })

  return {
    props: {
      technologies: allTechnologies
    }
  }
}
