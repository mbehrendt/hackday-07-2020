import Link from 'next/link'

import { useFetchUser } from 'lib/user'
import 'styles/global.css'

function MyApp({ Component, pageProps }) {
  const { user, loading } = useFetchUser()

  return <div className="bg-purple-800 pt-4">
    <div className="bg-white flex flex-col font-sans">
      <div className="container mx-auto px-8">
        <header className="flex flex-col sm:flex-row items-center justify-between py-6 relative">
          <Link href="/">
            <h3 className="cursor-pointer text-2xl font-bold uppercase text-blue-900">Hackday</h3>
          </Link>

          <nav className="hidden md:flex text-lg">
            <Link href="/">
              <a className="text-gray-800 hover:text-purple-300 py-3 px-6">Home</a>
            </Link>

            <Link href="/about">
              <a className="text-gray-800 hover:text-purple-300 py-3 px-6">About</a>
            </Link>

            {user && <Link href="/api/logout">
              <a className="text-gray-800 hover:text-purple-300 py-3 px-6">Logout</a>
            </Link>}

            {user && <Link href="/account">
              <a className="bg-purple-200 hover:bg-purple-300 rounded-full uppercase text-purple-700 py-3 px-6">
                <img width={25} className="inline-block mr-2 align-top rounded-full" src={user.picture} />
                My account
              </a>
            </Link>}

            {!loading && !user && <Link href="/api/login">
              <a className="bg-purple-200 hover:bg-purple-300 rounded-full uppercase text-purple-700 py-3 px-6">Sign Up</a>
            </Link>}
          </nav>
        </header>

        <main className="flex flex-col-reverse sm:flex-row jusitfy-between items-center py-12">
          <Component {...pageProps} />
        </main>
      </div>
    </div>
  </div>
}

export default MyApp
