import { useState, useReducer } from 'react'

import withAuth from 'components/with-auth'
import Frequency from 'components/frequency'
import TrainingDays from 'components/training-days'
import Maxes from 'components/maxes'
import Plan from 'components/plan'

export function Wizard() {
  const [step, setStep] = useState(1)
  const [frequency, setFrequency] = useState(1)
  const [plan, setPlan] = useState([])

  const [days, setDays] = useReducer((state, action) => (
    { ...state, ...action  }
  ), { 0: false, 1: false, 2: false, 3: false, 4: false, 5: false, 6: false })
  const [maxes, setMaxes] = useReducer((state, action) => (
    { ...state, ...action }
  ), { deadlift: null, squat: null, bench: null, ohp: null })

  const widthForStep = (current) => (
    { width: step > (current - 1) ? '100%' : '0%' }
  )

  const colorForStep = (current) => (
    step > (current - 1) ? 'bg-purple-800' : 'bg-gray-200'
  )

  const selectStep = (value) => {
    step >= value && setStep(value)
  }

  const save = async () => {
    try {
      const res = await fetch(`/api/wizard`, {
        method: 'POST',
        body: JSON.stringify({ frequency, days, maxes })
      }).then(res => res.json())

      setPlan(res.plan)
      setStep(step + 1)
    } catch {
      // TODO
    }
  }

  return (
    <div className="w-full">
      <div className="flex">
        <div className="w-1/4">
          <div className="relative mb-2">
            <a className="cursor-pointer" onClick={() => selectStep(1)}>
              <div className="w-10 h-10 mx-auto bg-purple-800 rounded-full flex"></div>
            </a>
          </div>

          <div className="text-xs text-center md:text-base">Frequency</div>
        </div>

        <div className="w-1/4">
          <div className="relative mb-2">
            <div
              className="absolute flex align-center items-center align-middle content-center"
              style={{ width: 'calc(100% - 2.5rem - 1rem)', top: '50%', transform: 'translate(-50%, -50%)' }}
            >
              <div className="w-full bg-gray-200 rounded items-center align-middle align-center flex-1">
                <div className="w-0 bg-purple-800 py-1 rounded" style={widthForStep(2)}></div>
              </div>
            </div>

            <a className="cursor-pointer" onClick={() => selectStep(2)}>
              <div className={`w-10 h-10 mx-auto ${colorForStep(2)} rounded-full flex items-center`}></div>
            </a>
          </div>

          <div className="text-xs text-center md:text-base">Training days</div>
        </div>

        <div className="w-1/4">
          <div className="relative mb-2">
            <div
              className="absolute flex align-center items-center align-middle content-center"
              style={{ width: 'calc(100% - 2.5rem - 1rem)', top: '50%', transform: 'translate(-50%, -50%)' }}
            >
              <div className="w-full bg-gray-200 rounded items-center align-middle align-center flex-1">
                <div className="w-0 bg-purple-800 py-1 rounded" style={widthForStep(3)}></div>
              </div>
            </div>

            <a className="cursor-pointer" onClick={() => selectStep(3)}>
              <div className={`w-10 h-10 mx-auto ${colorForStep(3)} rounded-full flex items-center`}></div>
            </a>
          </div>

          <div className="text-xs text-center md:text-base">Current 1RMs</div>
        </div>

        <div className="w-1/4">
          <div className="relative mb-2">
            <div 
              className="absolute flex align-center items-center align-middle content-center"
              style={{ width: 'calc(100% - 2.5rem - 1rem)', top: '50%', transform: 'translate(-50%, -50%)' }}
            >
              <div className="w-full bg-gray-200 rounded items-center align-middle align-center flex-1">
                <div className="w-0 bg-purple-800 py-1 rounded" style={widthForStep(4)}></div>
              </div>
            </div>

            <a className="cursor-pointer" onClick={() => selectStep(4)}>
              <div className={`w-10 h-10 mx-auto ${colorForStep(4)} rounded-full flex items-center`}></div>
            </a>
          </div>

          <div className="text-xs text-center md:text-base">Your plan</div>
        </div>
      </div>

      {step == 1 && <Frequency frequency={frequency} setFrequency={setFrequency} />}
      {step == 2 && <TrainingDays days={days} setDays={setDays} />}
      {step == 3 && <Maxes maxes={maxes} setMaxes={setMaxes} />}
      {step == 4 && <Plan plan={plan} />}

      {step <= 3 && <div className="text-center">
        <a
          onClick={() => step < 3 ? setStep(step + 1) : save()}
          className="cursor-pointer inline-block bg-purple-800 hover:bg-purple-700 rounded-full uppercase text-white py-3 px-6"
        >
          {step < 3 ? 'Next' : 'Save'}
        </a>
      </div>}
    </div>
  )
}

export default withAuth(Wizard)
