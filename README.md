# Getting Started

Install dependencies

```bash
yarn install
```

and then run the app locally

```bash
yarn dev
```

Make sure to change your environment variables in `.env.local`. The following variables exist (all of them are mandatory):

* DATOCMS_API_TOKEN
* FAUNA_SECRET
* AUTH0_CLIENT_ID
* AUTH0_CLIENT_SECRET
* AUTH0_COOKIE_SECRET