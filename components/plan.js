import PropTypes from 'prop-types'

import possibleDays from 'lib/days'

const Plan = ({ plan }) => {
  return (
    <div className="justify-center p-10">
      {plan.map(item => (
        <div key={item.day} className="bg-white rounded-lg w-2/3 mx-auto mb-10 p-4 shadow">
          <div>
            <span className="text-gray-900 relative inline-block date uppercase font-medium tracking-widest">
              {possibleDays[item.day]}
            </span>
            <div className="flex mb-2">
              <div className="w-2/12">
                {item.lifts.map(l => (
                  <span key={l.lift} className="text-sm text-gray-600 block">{l.lift}</span>
                ))}
              </div>
              <div className="w-1/12">
              </div>
              <div className="w-9/12">
                {item.lifts.map(l => (
                  <span key={l.lift} className="text-sm block">{l.sets.join(', ')}</span>
                ))}
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

Plan.propTypes = {
  plan: PropTypes.array.isRequired
}

export default Plan
