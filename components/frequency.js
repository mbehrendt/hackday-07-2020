import PropTypes from 'prop-types'

const Frequency = ({ frequency, setFrequency }) => {
  return (
    <div>
      <h4 className="text-gray-500 font-medium mt-12 text-center">
        How often pre week would you like to train?
      </h4>

      <div className="mt-10 w-1/3 mx-auto">
        {[1,2,3,4,5,6,7].map(t => {
          const borderClass = t == frequency ? 'border-blue-500' : ''

          return (
            <button
              key={t}
              onClick={() => setFrequency(t)}
              className={`flex mb-5 items-center justify-between w-full bg-white rounded-md border-2 ${borderClass} p-3 focus:outline-none`}
            >
              <label className="flex items-center">
                <input
                type="radio"
                className="form-radio h-5 w-5 text-blue-600"
                checked={t == frequency}
                onChange={() => setFrequency(t)}
              />
                <span className="ml-2 text-sm text-gray-700">{t} {t == 1 ? 'time' : 'times'} per week</span>
              </label>

              {[3,4,5,6].includes(t) && <span className="text-gray-600 text-sm">Recommended</span>}
            </button>
          )
        })}
      </div>
    </div>
  )
}

Frequency.propTypes = {
  frequency: PropTypes.number.isRequired,
  setFrequency: PropTypes.func.isRequired
}

export default Frequency
