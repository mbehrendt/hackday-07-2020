import PropTypes from 'prop-types'

import possibleDays from 'lib/days'

const TrainingDays = ({ days, setDays }) => {
  return (
    <div>
      <h4 className="text-gray-500 font-medium mt-12 text-center">
        On which days can you train for at least 45 minutes?
      </h4>

      <div className="mt-10 w-1/3 mx-auto">
        {possibleDays.map((day, index) => {
          const borderClass = days[index] ? 'border-blue-500' : ''

          return (
            <button
              key={index}
              onClick={() => setDays({ [index]: !days[index] })}
              className={`flex mb-5 items-center justify-between w-full bg-white rounded-md border-2 ${borderClass} p-3 focus:outline-none`}
            >
              <label className="flex items-center">
                <input
                  type="checkbox" className="form-radio h-5 w-5 text-blue-600"
                  checked={days[index]}
                  onChange={() => setDays({ [index]: !days[index] })}
                />
                <span className="ml-2 text-sm text-gray-700">{day}</span>
              </label>
            </button>
          )
        })}
      </div>
    </div>
  )
}

TrainingDays.propTypes = {
  days: PropTypes.object.isRequired,
  setDays: PropTypes.func.isRequired
}

export default TrainingDays
