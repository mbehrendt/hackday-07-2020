import PropTypes from 'prop-types'

const Maxes = ({ maxes, setMaxes }) => {
  const lifts = [
    { name: 'Deadlift', key: 'deadlift' },
    { name: 'Squat', key: 'squat' },
    { name: 'Bench press', key: 'bench' },
    { name: 'Overhead press', key: 'ohp' }
  ]

  return (
    <div>
      <h4 className="text-gray-500 font-medium mt-12 text-center">
        Please enter your current 1 rep maxes (estimate is fine) in KG
      </h4>

      <div className="mt-10 w-1/3 mx-auto">
        {lifts.map(lift => (
          <div key={lift.key} className="flex mb-5">
            <span className="text-sm w-1/2 border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">
              {lift.name}
            </span>
            <input
              onChange={(e) => setMaxes({ [lift.key]: e.target.value })}
              className="border border-2 rounded-r px-4 py-2 w-full" type="text"
            />
          </div>
        ))}
      </div>
    </div>
  )
}

Maxes.propTypes = {
  maxes: PropTypes.object.isRequired,
  setMaxes: PropTypes.func.isRequired
}

export default Maxes
