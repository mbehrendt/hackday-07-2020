import Router from 'next/router'
import { useEffect } from 'react'

export default function RedirectToLogin() {
  useEffect(() => {
    window.location.assign(`/api/login?redirectTo=${encodeURIComponent(Router.pathname)}`)
  })

  return (
    <div>Signing you in...</div>
  )
}
